# Reference: <https://postmarketos.org/vendorkernel>
# Maintainer:  Clayton Craft <clayton@craftyguy.net>
pkgname=linux-purism-librem5
# Note: _p#, where # is the purism version, e.g. the '2' in 'librem5.2'
pkgver=5.7.12_p1
pkgrel=0
_kernver=${pkgver%_p*}
_purismrel=${pkgver#*_p}
# <kernel ver>.<purism kernel release>
_purismver=${_kernver}+librem5.$_purismrel
pkgdesc="Purism Librem 5 phone kernel fork"
arch="aarch64"
_carch="arm64"
_flavor="purism-librem5"
url="https://source.puri.sm/Librem5/linux-next"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native"
makedepends="perl sed installkernel bash gmp-dev bc linux-headers elfutils-dev
	     devicepkg-dev bison flex openssl-dev"

# Source
_repository="linux-next"
# kconfig generated with: ARCH=arm64 make defconfig KBUILD_DEFCONFIG=librem5_defconfig
_config="config-$_flavor.$arch"
source="
	$pkgname-$_purismver.tar.gz::https://source.puri.sm/Librem5/$_repository/-/archive/pureos/$_purismver/$_repository-pureos-$_purismver.tar.gz
	$_config
"
builddir="$srcdir/$_repository-pureos-$_purismver"

prepare() {
	default_prepare
	REPLACE_GCCH=0 \
		. downstreamkernel_prepare
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS" \
		LOCALVERSION=".$_purismrel"
}

package() {
	downstreamkernel_package "$builddir" "$pkgdir" "$_carch" "$_flavor"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"

}
sha512sums="d53abc46e26fc589f554ed911481a61e07d3e18110b529a0d942553600937876f38a1cb0fd30b2b8d7967429ac91d331c9794439b4d088ee4e468194eb3ff131  linux-purism-librem5-5.7.12+librem5.1.tar.gz
ae47ca8feb81b241749d3172b2226a27f6ef347cbc53a1fd1f4d96683c54c0d80f604d492d6eab0f480b13df5f874c21cc81584bc591d460d732aa39a512b54b  config-purism-librem5.aarch64"
